const EXT_NAME = 'jupyterlab-workshop-extension';

import {
    JupyterFrontEnd,
    JupyterFrontEndPlugin
} from '@jupyterlab/application';

import { 
    NotebookPanel,
    INotebookModel,
    //INotebookTracker,
    //NotebookActions
} from '@jupyterlab/notebook';

import { 
    IDisposable,
    DisposableDelegate
} from '@lumino/disposable';

import {
    DocumentRegistry
} from '@jupyterlab/docregistry';

import {
    ToolbarButton,
    //sessionContextDialogs,
    //IWidgetTracker
} from '@jupyterlab/apputils';

import {
    ILauncher
} from '@jupyterlab/launcher';

import * as WidgetModuleType from '@jupyterlab/terminal/lib/widget';

/*
To do: 
Hook this up to Git at some point
Edit shortcuts
Where did I get the Lock Icon?


Initially I tried using the various INotebookTracker signals to automatically lock the cells when the notebook was opened/selected, but this was inconsistent:
(INotebookTracker).currentChanged.connect(() => {}
(INotebookTracker).widgetAdded.connect(() => {}
(INotebookTracker).selectionChanged.connect(() => {}
They didn't trigger when a notebook was opened, only when switching from one open notebook to another. There is probably some correct trigger to use (or I needed to implement some asynchronous function linked to e.g. widgetAdded).
Note: I wonder if these were undefined because they were not passed to optional/required? Should test this.

Buttons seemed like the next best option.
Technically Locking/Unlocking could use a single button and the same callback, since it is just using toggle. However, I could not figure out how to toggle the button icon. Using btnLockCells.toggleClass(iconClass) changed the class of the parent element not the actual button svg, leading to a messed up button with two icons.

Some fiddling with the cell element classes showed that simply removing jp-Notebook-cell prevented the cell from entering Edit Mode while still remaining rendered, and maintaining HTML interactions (e.g. child <detail> elements can still be expanded/collapsed). Code cells can still be edited, but they cannot be executed. Editing the editable/deletable metadata fixes that.

I tried making a custom launcher to contain a terminal and help file links at the top, but was stymied by the current category system.
*/

class RunAllButtons implements DocumentRegistry.IWidgetExtension<NotebookPanel, INotebookModel> {
    // access frontend from class
    constructor(app: JupyterFrontEnd) {
        this.app = app;  
        }
    readonly app: JupyterFrontEnd;
    
    // Create buttons
    createNew(panel: NotebookPanel, context: DocumentRegistry.IContext<INotebookModel>): IDisposable {
        // access Notebook commands
        const { commands } = this.app;
        
        // LockCells callback
        let cbLockCells = () => {
            // Toggle lock/unlock buttons
            btnLockCells.hide()
            btnUnlockCells.show()
            //commands.execute('notebook:deselect-all'); (I was hoping this would prevent a single cell from being selected and frozen, but it did not)
            for (let i = 0; i < panel.content.widgets.length; i++) {
                let cell = panel.content.widgets[i];
                let tags = cell.model.metadata.get("tags") as string[] || ["no tag"]; // convert PartialJSON to array of strings
                // If one of the tags is "Frozen", lock the cell by literally removing its identity as a jupyter element. Probably a better way?
                if (tags.indexOf("Frozen") > -1){
                    console.log("Freezing cell: ",cell);
                    cell.model.metadata.set("editable", "false");
                    cell.model.metadata.set("deletable", "false");
                    cell.node.classList.toggle("jp-Notebook-cell");
                }
            }
        }

        // UnlockCells callback
        let cbUnlockCells = () => {
            btnLockCells.show()
            btnUnlockCells.hide()
            for (let i = 0; i < panel.content.widgets.length; i++) {
                let cell = panel.content.widgets[i];
                let tags = cell.model.metadata.get("tags") as string[] || ["no tag"];
                if (tags.indexOf("Frozen") > -1){
                    console.log("Unfreezing cell: ",cell);
                    cell.model.metadata.set("editable", "true");
                    cell.model.metadata.set("deletable", "true");
                    cell.node.classList.toggle("jp-Notebook-cell"); 
                }
            }
        }
        // CreateTerminal callback
        let cbOpenTerminal = () => {
            // Would like to be able to choose where to dock this
            commands
              .execute('terminal:create-new')
              .then((terminal: WidgetModuleType.Terminal) => {
                this.app.shell.add(terminal, 'main');
              });
        }
        
        // Create LockCell button
        let btnLockCells = new ToolbarButton({
            className: 'btnLockCells',
            iconClass: 'ngs-LockCellsIcon',
            onClick: cbLockCells,
            tooltip: 'Lock all cells with the Frozen tag'
        });

        // Create UnlockCell button
        let btnUnlockCells = new ToolbarButton({
            className: 'btnUnlockCells',
            iconClass: 'ngs-UnlockCellsIcon',
            onClick: cbUnlockCells,
            tooltip: 'Unlock all cells with the Frozen tag'
        });
        
        // Create OpenTerminal button
        let btnOpenTerminal = new ToolbarButton({
            className: 'btnOpenTerminal',
            iconClass: 'jp-TerminalIcon',
            onClick: cbOpenTerminal,
            tooltip: 'Open a Terminal screen in a new tab'
        });


        // Add new buttons to toolbar
        panel.toolbar.insertItem(10, 'openTerminal', btnOpenTerminal);
        panel.toolbar.insertItem(10, 'lockCells', btnLockCells);
        panel.toolbar.insertItem(10, 'unlockCells', btnUnlockCells);
        // Initially hide unlock button
        btnUnlockCells.hide();
        
        
        // Return a delegate which can dispose our created button
        return new DisposableDelegate(() => {
            btnLockCells.dispose();
            btnUnlockCells.dispose();
            btnOpenTerminal.dispose();
        });
    }
}



/**
 * Initialization data for the jupyterlab-workshop-extension extension.
 */
const extension: JupyterFrontEndPlugin<void> = {
//  id: 'jupyterlab-workshop-extension',
    id: EXT_NAME,
    autoStart: true,
    optional: [ILauncher], // need this here or in requires, otherwise launcher is undefined
    activate: (app: JupyterFrontEnd, launcher: ILauncher | null) => {
        // Start message
        console.log(`JupyterLab extension ${EXT_NAME} is active.`);
        
        // Adding shortcuts
        app.commands.addKeyBinding({
          command: 'terminal:create-new',
          args: {},
          keys: ['Shift Tab'],
          selector: 'body'
        });
        
        /*
        // Custom launcher?
        // Doesn't seem possible yet due to "const KNOWN_CATEGORIES = ['Notebook', 'Console', 'Other']", short of makeing an entire launcher extension.
        // To be on top the command must be added to the Notebook category, which also does weird things to the icons since it is seen as a kernel.
            if (launcher) {
              launcher.add({
                command: 'terminal:create-new',
                category: 'Notebook',
                //iconClass: 'jp-TerminalIcon',
                rank: 0
              });
            }
        */
        // Register our extension
        app.docRegistry.addWidgetExtension('notebook', new RunAllButtons(app));
  }
};

export default extension;
